function setRem() {
  const WIDTH = 375;
  let clientWidth = document.documentElement.clientWidth;
  document.documentElement.style.fontSize = (16 * clientWidth) / WIDTH + "px";
}
setRem();
window.onresize = () => {
  setRem();
};

