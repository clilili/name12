// module.exports = {
// 	lintOnSave:false
// }
// const path = require("path");
// const px2vw = require("postcss-px-to-viewport");
module.exports = {
	lintOnSave:false,
  // css 配置
    css: {
      loaderOptions: {
        postcss: {
          plugins: [
            require("postcss-pxtorem")({
              rootValue:16,
              propList: ["*", "!font-size"],
            }),
          ],
        },
      },
    },
    devServer: {
      port: 8080,
      host: 'localhost',
      open: true,
      https: false,
      proxy: {
        '/api': {
          target: 'https://dahua0822-api.herokuapp.com',
          ws: true,
          changeOrigin: true,
          pathRewrite: {
            '^/api': ''
          }
        }
      }
  
    }
  
  };